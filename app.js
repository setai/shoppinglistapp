var express         = require("express"),
    mongoose        = require("mongoose"),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    expressSanitizer= require("express-sanitizer"),
    User            = require("./models/user"),
    passport        = require("passport"),
    LocalStrategy   = require("passport-local"),
    flash           = require("connect-flash"),
    app = express();

// Requiring routes
var basketRoutes        = require("./routes/baskets"),
    indexRoutes         = require("./routes/index");

// APP CONFIG
// {useMongoClient: true} makes 'open() is depricated' warning go away
mongoose.connect(process.env.DATABASEURLSL, {useMongoClient: true});
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride("_method")); // keyword _method = PUT
app.use(expressSanitizer());
app.use(flash());   // must come before your passport configuration (both use sessions?)

// Passport configuration
app.use(require("express-session")({
    secret: process.env.SECRET, 
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate())); // Comes from passport-local-mongoose package
passport.serializeUser(User.serializeUser());  // Comes from passport-local-mongoose package
passport.deserializeUser(User.deserializeUser());  // Comes from passport-local-mongoose package

// Whatever we put in res.locals is available in ejs-templates. 
// This is a middleware that is run for every single route.
app.use(function(req, res, next) {
  res.locals.currentUser = req.user;        // to show 'Logged in as ...' in header.ejs
  res.locals.error = req.flash("error");    // to show flash error messages in header.ejs
  res.locals.success = req.flash("success");// to show flash success messages in header.ejs
  next();
});


// ROUTES
app.use("/", indexRoutes);   // or app.use("/", indexRoutes);
app.use("/baskets", basketRoutes); // Makes path "/baskets" default value for all basketRoutes.





app.listen(process.env.PORT, process.env.IP, function() {
    console.log("Server is running");
});