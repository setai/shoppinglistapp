var Basket = require("../models/basket");

// all the middleware goes here
var middlewareObj = {};

/*
middlewareObj.checkCommentOwnership = function() {} is a synonym to
middlewareObj = {checkCommentOwnership: function() {} }
*/

// Middleware to check if a user is logged in.
middlewareObj.isLoggedIn = function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.flash("error", "Sinun tulee olla kirjautunut. / Please, login to do that.");
    res.redirect("/login");
};

middlewareObj.checkBasketOwnership = function(req, res, next) {
    if (req.isAuthenticated()) {
        Basket.findById(req.params.id, function(err, foundBasket) {
            if (err || !foundBasket) {
                req.flash("error", "Koria ei löydy. / Basket not found.");
                res.redirect("back");
            } else {
                // foundBasket.author.id is a Mongoose object
                // req.user._id is a String, so '===' won't work between them.
                if (foundBasket.author.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash("error", "Sinulta puuttuu oikeus. / You don't have permission to do that");
                    res.redirect("back");
                }
            }
        });
    } else {
        req.flash("error", "Sinun tulee olla kirjautunut. / You need to be logged in.");
        res.redirect("back");
    }
};

module.exports = middlewareObj;