var mongoose = require("mongoose");

// Schema setup
var basketSchema = new mongoose.Schema({
    title: String,
    items: [],
    isDone: [],
    created: {type: Date, default: Date.now},
    author: {
        id: { // id is a reference to User model id
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        username: String
    }
});

module.exports = mongoose.model("Basket", basketSchema);
