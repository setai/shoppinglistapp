$(".basket-item ul").on("click", "li", function() {
	$(this).toggleClass("completed")
});

$(".basket-item ul").on("click", "span", function(e) {
	$(this).parent().fadeOut(500,function(){
		$(this).remove();
	});
	event.stopPropagation();
});

$(".basket-item input[type='text']").keypress(function(event) {
	if (event.which === 13) {
		var item = $(this).val();
		if (item != "") {
			$(this).val("");
	  	$("ul").append("<li> " + item + "<span><i class='fa fa-window-close'></i></span></li>");

		}
	}
});

$(".fa-plus").click(function(){
	$(".basket-item input[type='text'").fadeToggle()
});

$(".post_create_btn").click(function() {
	postRefreshPage("/baskets");
});

$(".post_edit_btn").click(function() {
  var id = $("#basket_id").val();
	postRefreshPage("/baskets/" + id + "?_method=put");
});

$(".post_delete_btn").click(function() {
  var id = $("#basket_id").val();
	postRefreshPage("/baskets/" + id + "?_method=delete");
});

function postRefreshPage (action) {
  var theForm, newInput;
  // Start by creating a <form>
  theForm = document.createElement('form');
  theForm.action = action;
  theForm.method = 'post';
  // Next create the <input>s in the form and give them names and values
  newInput = document.createElement('input');
  newInput.type = 'hidden';
  newInput.name = 'basket[title]';
  newInput.value = $('input').first().val();
  theForm.appendChild(newInput);
  
  $("li").each(function(index) {
  	newInput = document.createElement('input');
  	newInput.type = 'hidden';
  	newInput.name = 'basket[items][]';
  	newInput.value = $(this).text();
  	theForm.appendChild(newInput);
  	newInput = document.createElement('input');
  	newInput.type = 'hidden';
  	newInput.name = 'basket[isDone][]';
  	if ($(this).hasClass('completed')) {
  		newInput.value = 'completed';
  	} else {
  		newInput.value = '';
  	}
  	theForm.appendChild(newInput);
  });
  
  // ...and it to the DOM...
  document.getElementById('hidden_form_container').appendChild(theForm);
  // ...and submit it
  theForm.submit();
}