var express = require("express");
var router  = express.Router();
var Basket = require("../models/basket");
// When a directory is required, it will automatically require the content of the index.js.
// This could be written require("../middleware/index.js") as well.
var middleware = require("../middleware");

// RESTFUL ROUTES

// index route
router.get("/", middleware.isLoggedIn, function(req, res) {
    Basket.find({}, function(err, baskets) {
        if (err) {
            console.log(err);
        } else {
            res.render("baskets/index", {baskets: baskets});
        }
    });
});

// new route
router.get("/new", middleware.isLoggedIn, function(req, res) {
   res.render("baskets/new") ;
});

// create route
router.post("/", middleware.isLoggedIn, function(req,res) {
    req.body.basket.body = req.sanitize(req.body.basket.body);
    Basket.create(req.body.basket, function(err, basket) {
       if (err) {
           res.render("/baskets/new");
       } else {
           // add user to the basket
           basket.author.id = req.user._id;
           basket.author.username = req.user.username;
           // save basket
           basket.save();
           req.flash("success", "Uusi kori lisätty. / New basket added.");
           res.redirect("/baskets");
       }
   }); 
});

// edit route
router.get("/:id/edit", middleware.checkBasketOwnership, function(req, res) {
    Basket.findById(req.params.id, function(err, foundBasket) {
        if (err || !foundBasket) {
            req.flash("error", "Koria ei löydy. / Basket not found.");
            res.redirect("back");
        } else {
            res.render("baskets/edit", {basket: foundBasket});
        }
    });
});

// update route
router.put("/:id", middleware.checkBasketOwnership, function(req, res) {
    req.body.basket.body = req.sanitize(req.body.basket.body);
    // Handle case that there are no items in basket.
    if (typeof req.body.basket.items === 'undefined') {
        req.body.basket.items = [];
        req.body.basket.isDone = [];
    }
    Basket.findByIdAndUpdate(req.params.id, req.body.basket, function(err, updatedBasket) {
        if (err || !updatedBasket) {
            res.redirect("/baskets");
        } else {
            req.flash("success", "Kori tallennettu. / Basket saved.");
            res.redirect("/baskets/" + req.params.id + "/edit");
        }
        
    });
});

// delete route
router.delete("/:id", middleware.checkBasketOwnership, function(req, res) {
   Basket.findByIdAndRemove(req.params.id, function(err) {
      if (err) {
          res.redirect("/baskets");
      } else {
          req.flash("success", "Kori poistettu. / Basket deleted.");
          res.redirect("/baskets");
      }
   });
});

module.exports = router;