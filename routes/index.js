var express = require("express");
var router  = express.Router();
var passport = require("passport");
var User = require("../models/user");

// root route
router.get("/", function(req, res) {
    res.render("login");
});


// show register form
router.get("/register", function(req, res) {
    res.render("register", {page: "register"});
});

// handle sign up logic
router.post("/register", function(req, res) {
    var newUser = new User({username: req.body.username});
    User.register(newUser, req.body.password, function(err, user) {
        if (err) {
            return res.render("register", {error: err.message});
        }
        passport.authenticate("local")(req, res, function() {
            req.flash("success", "Succesfully signed up! Nice to meet you " + user.username + ". You can start adding new shopping baskets.");
            res.redirect("/baskets");
        });
    });
});

// show login form
router.get("/login", function(req, res) {
    res.render("login", {page: "login"});
});

// handling login logic
router.post("/login", passport.authenticate("local", {
        successRedirect: "/baskets",
        failureRedirect: "/login",
        successFlash: "Tervetuloa / Welcome - Voit muokata ostoskorejasi! / Update your shopping baskets now!",  // tells the method to use a flash message in the event of success
        failureFlash: "Virheellinen tunnus tai salasana. / Invalid username or password."  // tells the method to use a flash message in the event of failure
        // failureFlash: true would give a default error message "Invalid username or password". successFlash seems to not have a default message.
    }), function(req, res) {
});

// logout route
router.get("/logout", function(req, res) {
    req.logout();
    req.flash("success", "Olet kirjautunut ulos. / Logged you out.");
    res.redirect("/login");
});

module.exports = router;